package no.ntnu.sondrmal.cardgame;

import java.util.*;

/**
 * Class representing a hand of cards
 * imports java.util.*
 *
 * @author Sondre Malerud
 */
public class HandOfCards {
    private final List<PlayingCard> hand;
    private final char[] suit = {'S', 'H', 'D', 'C'};

    /**
     * Constructor that takes in a List of PlayingCards and assigns it to this hand
     *
     * @param hand List, is the list representing the hand of cards
     */
    public HandOfCards(List<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     * Constructor with no parameter, creating an empty arraylist
     */
    public HandOfCards() {
        this.hand = new ArrayList<>();
    }

    /**
     * Method checking if the hand of cards has a flush (5 cards of the same suit)
     *
     * @return boolean, true if the hand has a flush, false otherwise
     */
    public boolean hasFlush() {
        if (this.hand.isEmpty()) return false;
        for (int i = 0; i < 4; i++) {
            int finalI = i;
            if (this.hand.stream().filter(c -> c.getSuit() == suit[finalI]).count() > 4) return true;
        }
        return false;
    }

    /**
     * Method checking if the hand of cards has four of a kind (4 cards of the same face)
     *
     * @return boolean, true if the hand has four of a kind, false otherwise
     */
    public boolean hasFourOfAKind() {
        if (this.hand.size() < 4) return false;
        for (int i = 1; i < 14; i++) {
            int finalI = i;
            if (this.hand.stream().filter(c -> c.getFace() == finalI).count() == 4) return true;
        }
        return false;
    }

    /**
     * Returns a sum of every card's face value in this hand
     *
     * @return int, sum of every card's face
     */
    public Integer sumOfCards() {
        return this.hand.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }


    /**
     * Returns the total number of cards of hearts in this hand
     *
     * @return int, the total number of cards of hearts
     */
    public int numberOfHearts() {
        return (int) this.hand.stream().filter(c -> c.getSuit() == 'H').count();
    }

    /**
     * Returns a list of cards of hearts.
     *
     * @return List, of the hand's card of hearts.
     */
    public List<PlayingCard> cardsOfHearts() {
        return this.hand.stream().filter(c -> c.getSuit() == 'H').toList();
    }


    /**
     * Method that tells whether the hand includes a queen of spades
     *
     * @return boolean, true if the hand includes a queen of spades, false otherwise
     */
    public boolean includesQueenOfSpades() {
        return this.hand.stream().anyMatch(c -> c.getSuit() == 'S' && c.getFace() == 12);
    }


    /**
     * Adds a list of PlayingCards to the hand of cards
     *
     * @param cards is the list of cards that will be added to the hand
     */
    public void addCardsToHand(List<PlayingCard> cards) {
        this.hand.addAll(cards);
    }

    /**
     * Returns this hand
     *
     * @return List, this hand.
     */
    public List<PlayingCard> getHand() {
        return this.hand;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        hand.forEach(c -> s.append(c).append(" "));
        return s.toString();
    }
}
