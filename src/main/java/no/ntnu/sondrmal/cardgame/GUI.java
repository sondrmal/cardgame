package no.ntnu.sondrmal.cardgame;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * GUI class that creates the entirety of the applications front end.
 * Imports javafx.*
 *
 * @author Sondre Malerud
 * @version 0.0.2
 */
public class GUI extends Application {

    Button dealHand;
    Button checkHand;
    DeckOfCards deck = new DeckOfCards();
    HandOfCards hand = new HandOfCards();
    Font Title = new Font("Franklin Gothic Medium", 50);
    Font Subtitle = new Font("Franklin Gothic Medium", 30);
    Font Normal = new Font("Franklin Gothic Medium", 20);
    Text sumOfFacesResult;
    Text flushResult;
    Text cardsOfHeartsResult;
    Text queenOfSpadesResult;
    ImageView showCard1, showCard2, showCard3, showCard4, showCard5;
    HBox cards;


    @Override
    public void start(Stage stage) {

        Group root = new Group();
        Scene scene = new Scene(root, 1200, 900, Color.WHITESMOKE);

        // creating menubar
        MenuBar menuBar = new MenuBar();
        menuBar.prefWidthProperty().bind(scene.widthProperty());

        Menu fileMenu = new Menu("File");
        Menu editMenu = new Menu("Edit");

        MenuItem exitItem = new MenuItem("Exit");
        MenuItem settingsItem = new MenuItem("Settings");
        MenuItem newItem = new MenuItem("New");
        exitItem.setOnAction(e -> System.exit(0));
        settingsItem.setDisable(true);
        newItem.setDisable(true);

        //functionality for these two can be added later
        MenuItem copyItem = new MenuItem("Copy");
        MenuItem pasteItem = new MenuItem("Paste");
        copyItem.setDisable(true);
        pasteItem.setDisable(true);

        fileMenu.getItems().addAll(newItem, settingsItem, exitItem);
        editMenu.getItems().addAll(copyItem, pasteItem);

        menuBar.getMenus().addAll(fileMenu, editMenu);


        // setting icon for the application
        Image icon = new Image("logo.png");
        stage.getIcons().add(icon);
        stage.setTitle("Card Game");
        stage.setResizable(false);

        //the text that will welcome the user
        Text welcome = new Text("Welcome to the card game app! ♠♥♦♣");
        welcome.setX(200);
        welcome.setY(80);
        welcome.setFont(Title);

        //the text above the user's current hand of cards
        Text staticHandText = new Text("Your current hand:\n\n");
        staticHandText.setFont(Subtitle);
        staticHandText.setX(242);
        staticHandText.setY(304);

        //the text that will show the hand of cards
        Text handText = new Text();
        handText.setFont(Normal);
        handText.setWrappingWidth(500);
        handText.setX(120);
        handText.setY(450);

        //big rectangle that the hand of cards will be showcased inside of
        Rectangle rectangle = new Rectangle();
        rectangle.setX(60);
        rectangle.setY(110);
        rectangle.setWidth(600);
        rectangle.setHeight(600);
        rectangle.setFill(Color.WHITE);
        rectangle.setStroke(Color.GREY);
        rectangle.setStrokeWidth(1);

        //button for getting dealt 5 cards from the card deck
        dealHand = new Button();
        dealHand.setText("Deal hand");
        dealHand.setFont(Subtitle);
        dealHand.setMinSize(250, 70);
        dealHand.setLayoutX(800);
        dealHand.setLayoutY(200);
        dealHand.setOnAction(e -> {
            if (deck.dealHand(5, hand)) {
                showCard1 = new ImageView(this.hand.getHand().get(0).getImage());
                showCard2 = new ImageView(this.hand.getHand().get(1).getImage());
                showCard3 = new ImageView(this.hand.getHand().get(2).getImage());
                showCard4 = new ImageView(this.hand.getHand().get(3).getImage());
                showCard5 = new ImageView(this.hand.getHand().get(4).getImage());

                cards = new HBox();
                cards.getChildren().addAll(showCard1, showCard2, showCard3, showCard4, showCard5);
                cards.setSpacing(10);
                cards.setMaxSize(100, 100);
                cards.setAlignment(Pos.BOTTOM_CENTER);
                VBox vBox = new VBox();
                vBox.getChildren().addAll(staticHandText, cards);
                vBox.setSpacing(20);
                vBox.setAlignment(Pos.CENTER);

                StackPane square = new StackPane();
                square.getChildren().addAll(rectangle, vBox);
                square.setLayoutX(60);
                square.setLayoutY(110);

                root.getChildren().add(square);
            } else {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, "You have drawn all cards from the deck! Relaunch the app to try another shuffled deck.");
                alert.setHeaderText("No more cards available");
                alert.setTitle("No more cards available");
                alert.show();
            }
        });

        //button for checking the hand for various bonuses and information
        checkHand = new Button();
        checkHand.setText("Check hand");
        checkHand.setFont(Subtitle);
        checkHand.setMinSize(250, 70);
        checkHand.setLayoutX(800);
        checkHand.setLayoutY(325);
        checkHand.setOnAction(e -> {
            sumOfFacesResult.setText(Integer.toString(hand.sumOfCards()));
            flushResult.setText(Boolean.toString(hand.hasFlush()));
            cardsOfHeartsResult.setText(hand.cardsOfHearts().toString());
            queenOfSpadesResult.setText(Boolean.toString(hand.includesQueenOfSpades()));
        });

        //implementing the sum of faces information functionality
        Text sumOfTheFaces = new Text("Sum of the faces: ");
        sumOfTheFaces.setFont(Normal);
        sumOfTheFaces.setX(70);
        sumOfTheFaces.setY(745);

        Rectangle sumOfFacesRect = new Rectangle();

        sumOfFacesRect.setHeight(30);
        sumOfFacesRect.setWidth(55);
        sumOfFacesRect.setFill(Color.WHITE);
        sumOfFacesRect.setStroke(Color.GREY);
        sumOfFacesRect.setStrokeWidth(1);

        sumOfFacesResult = new Text();

        StackPane sumOfFacesStack = new StackPane();
        sumOfFacesStack.getChildren().addAll(sumOfFacesRect, sumOfFacesResult);
        sumOfFacesStack.setLayoutX(310);
        sumOfFacesStack.setLayoutY(725);


        //implementing the flush information functionality
        Text flush = new Text("Flush? ");
        flush.setFont(Normal);
        flush.setX(70);
        flush.setY(800);

        Rectangle flushRect = new Rectangle();
        flushRect.setHeight(30);
        flushRect.setWidth(55);
        flushRect.setFill(Color.WHITE);
        flushRect.setStroke(Color.GREY);
        flushRect.setStrokeWidth(1);

        flushResult = new Text();

        StackPane flushStack = new StackPane();
        flushStack.getChildren().addAll(flushRect, flushResult);
        flushStack.setLayoutX(310);
        flushStack.setLayoutY(778);


        //implementing the card of hearts information functionality
        Text cardsOfHearts = new Text("Cards of hearts: ");
        cardsOfHearts.setFont(Normal);
        cardsOfHearts.setX(390);
        cardsOfHearts.setY(745);

        Rectangle cardsOfHeartsRect = new Rectangle();
        cardsOfHeartsRect.setHeight(100);
        cardsOfHeartsRect.setWidth(110);
        cardsOfHeartsRect.setFill(Color.WHITE);
        cardsOfHeartsRect.setStroke(Color.GREY);
        cardsOfHeartsRect.setStrokeWidth(1);

        cardsOfHeartsResult = new Text();
        cardsOfHeartsResult.setWrappingWidth(100);

        StackPane cardsOfHeartsStack = new StackPane();
        cardsOfHeartsStack.getChildren().addAll(cardsOfHeartsRect, cardsOfHeartsResult);
        cardsOfHeartsStack.setLayoutX(550);
        cardsOfHeartsStack.setLayoutY(725);


        //implementing the check for queen of spades functionality
        Text queenOfSpades = new Text("Queen of spades in hand? ");
        queenOfSpades.setFont(Normal);
        queenOfSpades.setX(70);
        queenOfSpades.setY(850);

        Rectangle queenOfSpadesRect = new Rectangle();
        queenOfSpadesRect.setHeight(30);
        queenOfSpadesRect.setWidth(55);
        queenOfSpadesRect.setFill(Color.WHITE);
        queenOfSpadesRect.setStroke(Color.GREY);
        queenOfSpadesRect.setStrokeWidth(1);

        queenOfSpadesResult = new Text();

        StackPane queenOfSpadesStack = new StackPane();
        queenOfSpadesStack.getChildren().addAll(queenOfSpadesRect, queenOfSpadesResult);
        queenOfSpadesStack.setLayoutX(310);
        queenOfSpadesStack.setLayoutY(830);

        //adding all nodes to the group
        root.getChildren().addAll(menuBar, welcome, rectangle, checkHand, staticHandText, dealHand);
        root.getChildren().addAll(queenOfSpades, cardsOfHearts, flush, sumOfTheFaces, handText);
        root.getChildren().addAll(flushStack, sumOfFacesStack, cardsOfHeartsStack, queenOfSpadesStack);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}