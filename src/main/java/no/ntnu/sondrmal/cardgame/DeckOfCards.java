package no.ntnu.sondrmal.cardgame;

import java.util.*;

/**
 * Class representing a deck of 52 unique cards.
 * Imports java.util.*
 *
 * @author Sondre Malerud
 */
public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private final List<PlayingCard> deck;

    /**
     * Constructor that creates all 52 unique cards in a standard deck of cards, and adds them to the deck list
     */
    public DeckOfCards() {
        deck = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            for (int j = 1; j < 14; j++) {
                deck.add(new PlayingCard(suit[i], j));
            }
        }
        Collections.shuffle(this.deck); //shuffles the deck so we do not need to worry about implementing randomness in later methods
    }

    /**
     * Method that deals a "n" number of cards to a specified hand object.
     * Dealt cards will get removed from the deck.
     *
     * @param n    int, is the number of cards that will be dealt
     * @param hand HandOfCards, is the hand that will receive the dealt cards
     * @return boolean, false if HandOfCards equals null or hand has 5 or more cards, true otherwise.
     */
    public boolean dealHand(int n, HandOfCards hand) {
        if (this.deck.size() < 5) return false;
        if (hand == null) return false;
        if (hand.getHand().size() >= 5) hand.getHand().clear();

        List<PlayingCard> cards = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            cards.add(this.deck.get(0));
            this.deck.remove(0);
        }
        hand.addCardsToHand(cards);
        return true;
    }

}
