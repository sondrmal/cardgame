package no.ntnu.sondrmal.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {

    @Test
    @DisplayName("Making sure flushes will never happen with <5 cards")
    void flushWith4Cards() {
        DeckOfCards deck = new DeckOfCards();
        HandOfCards hand = new HandOfCards();
        deck.dealHand(4, hand);
        assertFalse(hand.hasFlush());
    }

    @Test
    @DisplayName("Checking that flush works as intended")
    void flushWorks() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('H', 2));
        cards.add(new PlayingCard('H', 3));
        cards.add(new PlayingCard('H', 4));
        cards.add(new PlayingCard('H', 5));
        HandOfCards hand = new HandOfCards(cards);
        assertTrue(hand.hasFlush());
    }

    @Test
    @DisplayName("Checking that four of a kind returns false when it's < 4 of a kind")
    void lessThanFourOfAKindDoesNotWork() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('S', 1));
        cards.add(new PlayingCard('D', 1));
        cards.add(new PlayingCard('H', 2));
        cards.add(new PlayingCard('H', 5));
        HandOfCards hand = new HandOfCards(cards);
        assertFalse(hand.hasFourOfAKind());
    }

    @Test
    @DisplayName("Checking that four of a kind works as intended")
    void fourOfAKindWorks() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('S', 1));
        cards.add(new PlayingCard('D', 1));
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('H', 5));
        HandOfCards hand = new HandOfCards(cards);
        assertTrue(hand.hasFourOfAKind());
    }

    @Test
    @DisplayName("Checking that sum method works as intended")
    void sumMethodWorks() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('S', 1));
        cards.add(new PlayingCard('D', 1));
        cards.add(new PlayingCard('H', 1));
        cards.add(new PlayingCard('H', 5));
        HandOfCards hand = new HandOfCards(cards);
        assertEquals(9, hand.sumOfCards());
    }

    @Test
    @DisplayName("Checking that the queen of spades method works")
    void noQueenOfSpadesGivesFalse() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('H', 1));
        HandOfCards hand = new HandOfCards(cards);
        assertFalse(hand.includesQueenOfSpades());

    }

    @Test
    @DisplayName("Checking that the queen of spades method works")
    void queenOfSpadesGivesTrue() {
        List<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard('S', 12));
        HandOfCards hand = new HandOfCards(cards);
        assertTrue(hand.includesQueenOfSpades());

    }
}

